# Setup Bash Aliases

This guide will help you set up custom bash aliases by copying the `.bash_aliases` file to your home directory and configuring your `.bashrc` file to source it.

## Steps

1. **Copy the `.bash_aliases` file to your home directory**

    ```sh
    cp .bash_aliases ~/
    ```

2. **Edit your `.bashrc` file to include the following lines**

    Open your `.bashrc` file with your preferred text editor. For example, using `nano`:

    ```sh
    nano ~/.bashrc
    ```

    Add the following lines at the end of the `.bashrc` file:

    ```sh
    # Source the .bash_aliases file if it exists
    if [ -f ~/.bash_aliases ]; then
        . ~/.bash_aliases
    fi
    ```

3. **Reload your `.bashrc` file**

    After saving and closing the `.bashrc` file, run the following command to reload it:

    ```sh
    source ~/.bashrc
    ```

## Additional Information

- The `.bash_aliases` file is a convenient way to manage your bash aliases separately from the main `.bashrc` file.
- Any aliases defined in the `.bash_aliases` file will be available in your shell sessions after completing the steps above.

