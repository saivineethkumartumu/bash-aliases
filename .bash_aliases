alias s='sudo' # Alias for common commands
alias v='vim'
alias h='htop'
alias n='nano'
alias src='source ~/.bashrc'
alias r='rm -rf'
alias now='date '\''+%A %d %B %Y - %T'\'''
mc() { mkdir -p -- "$1" && cd -P -- "$1"; } # create a folder and move into that folder
alias rd='rmdir'
alias diskusage='df -h'
alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'
### Create htaccess Hash
htaccess() {
  if [[ -z "$1" ]]; then
    echo "Usage: htaccess <username>"
    return 1
  fi

  # Generate a random 16-character password #nospecialChar
  password=$(tr -dc 'A-Za-z0-9' </dev/urandom | head -c 16)
  echo "$password" | htpasswd -c -i .htpasswd "$1"

  echo "===========================Contents of .htpasswd==========================="
  cat .htpasswd

  echo "Generated password for $1: $password"
  if [[ -f .htaccess ]]; then
    echo "Found .htaccess file, removing it."
    rm .htaccess
  else
    echo "No .htaccess file found."
  fi
}
### More Jump down
alias 1d='cd ..'
alias 2d='cd ../..'
alias 3d='cd ../../..'
alias 4d='cd ../../../..'
alias 5d='cd ../../../../..'
alias 6d='cd ../../../../../..'
# package management
alias install='sudo apt install'
alias remove='sudo apt remove --purge'
alias afix='sudo apt -f install'
alias update='sudo apt update'
alias upgrade='sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y'
alias snapi='sudo snap install'
alias snapr='sudo snap remove'
alias snapc='sudo snap install --classic'
#network
alias whatismyip='echo $(wget -qO - https://api.ipify.org)'
alias localip="ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'"
alias listenports='netstat -tulanp | grep LISTEN'
alias allports='netstat -tulanp'
alias netstat80="netstat -plan|grep :80|awk {'print $5'}|cut -d: -f 1|sort|uniq -c|sort -nk 1"
alias netstatports="netstat -nape --inet"
alias netstatpid="netstat -tlnp"
alias netstatapps="netstat -lantp | grep -i stab | awk -F/ '{print $2}' | sort | uniq"
alias connections80='netstat -anp |grep 80 |wc -l'
alias connections443='netstat -anp |grep 443 |wc -l'
#Services
alias status='sudo systemctl status'
alias start='sudo systemctl start'
alias restart='sudo systemctl restart'
alias stop='sudo systemctl stop'
alias jctl='sudo journalctl -u'
#git
alias gc='git clone'
alias gch='git checkout'
alias ga='git add'
alias gcm='git commit -m'
alias gdiff='git diff'
alias gpush='git push -u origin'
alias gpull='git pull'
alias grebase='git rebase'
alias gusername='git config --global user.name'
alias gemail='git config --global user.email'
alias gradd='git remote add'
alias grrename='git remote rename'
#Docker
alias db='docker build'
alias dps='docker ps'
alias dpsa='docker ps -a'
alias di='docker images'
alias dr='docker rm'
alias ds='docker stop'
alias drmi='docker rmi'
dexec() { docker exec -it "$1" /bin/bash;}
alias drmia='docker rmi $(docker images -aq)'
alias dc='docker-compose'
alias dcu='docker-compose up -d'
alias dcd='docker-compose down '
alias dsp='docker system prune --all'
#terraform
alias ti='terraform init'
alias tp='terraform plan'
alias ta='terraform apply'
alias tfmt='terraform fmt'
alias tval='terraform validate'
alias tt='terraform taint'
alias tut='terraform untaint'
alias tshow='terraform show'
alias trefresh='terraform refresh'
alias timport='terraform import'
#Ansible
alias ans='ansible'
alias ap='ansible-playbook'
alias ag='ansible-galaxy'
# Kubernetes commands
alias k='kubectl'
alias ka='kubectl apply -f'
alias kpa='kubectl patch -f'
alias ked='kubectl edit'
alias ksc='kubectl scale'
alias kex='kubectl exec -i -t'
alias kg='kubectl get'
alias kga='kubectl get all'
alias kgall='kubectl get all --all-namespaces'
alias kinfo='kubectl cluster-info'
alias kdesc='kubectl describe'
alias ktp='kubectl top'
alias klo='kubectl logs -f'
alias kn='kubectl get nodes'
alias kpv='kubectl get pv'
alias kpvc='kubectl get pvc'